import { Injectable } from '@nestjs/common';
import { Message, TasksDTOCoreApiModel } from '@pilf-onboarding/api-interfaces';

@Injectable()
export class AppService {
  getData(): Message {
    return { message: 'Welcome to api!' };
  }

  getTasks(): TasksDTOCoreApiModel {
    return {
      received_tasks: [
        {
          information: {
            task_id: 'd8695338-e4ae-4e80-9347-f11a211aa5f8',
            author_id: 'manu',
            created_at: '2022-03-22T15:40:04.220229Z',
            updated_at: '2022-03-22T15:40:04.220229Z',
            title: 'task 2',
            attachments: [],
            body: 'some more description',
          },
          recipient_user: {
            user_id: 'manu',
            status: 'OPEN',
            created_at: '2022-03-22T15:40:04.220223Z',
            updated_at: '2022-03-22T15:40:04.220223Z',
          },
        },
        {
          information: {
            task_id: 'df6a2035-4df3-4ec5-aca3-22723660de36',
            author_id: 'marco',
            created_at: '2022-03-10T15:26:26.823967Z',
            updated_at: '2022-03-10T15:26:26.823967Z',
            title: 'Test',
            attachments: [],
            body: 'Test',
          },
          recipient_user: {
            user_id: 'manu',
            status: 'OPEN',
            created_at: '2022-03-10T15:26:26.823955Z',
            updated_at: '2022-03-10T15:26:26.823955Z',
          },
        },
        {
          information: {
            task_id: '1267b156-5f88-4cb0-85a2-4a66b0491cdf',
            author_id: 'manu',
            created_at: '2022-03-22T15:40:27.110515Z',
            updated_at: '2022-03-22T15:40:27.110515Z',
            title: 'task 3',
            attachments: [],
            body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
          },
          recipient_user: {
            user_id: 'manu',
            status: 'OPEN',
            created_at: '2022-03-22T15:40:27.110503Z',
            updated_at: '2022-03-22T15:40:27.110503Z',
          },
        },
        {
          information: {
            task_id: '2839bcb1-2dce-4b51-980a-7ea49481017c',
            author_id: 'manu',
            created_at: '2022-03-03T14:54:54.774428Z',
            updated_at: '2022-03-03T14:54:54.774428Z',
            title: 'test',
            attachments: [],
          },
          recipient_user: {
            user_id: 'manu',
            status: 'OPEN',
            created_at: '2022-03-03T14:54:54.774420Z',
            updated_at: '2022-03-03T14:54:54.774420Z',
          },
        },
        {
          information: {
            task_id: '46a7fb6d-c856-46cf-92e2-c6c17f7d22f3',
            author_id: 'dario',
            created_at: '2022-03-08T16:35:37.871644Z',
            updated_at: '2022-03-08T16:35:37.871644Z',
            title: 'd',
            attachments: [],
          },
          recipient_user: {
            user_id: 'manu',
            status: 'OPEN',
            created_at: '2022-03-08T16:35:37.871637Z',
            updated_at: '2022-03-08T16:35:37.871637Z',
          },
        },
      ],
      distributed_tasks: [
        {
          information: {
            task_id: '3515c24d-eda3-494a-9e1c-526cc095a12c',
            author_id: 'manu',
            created_at: '2022-03-22T15:39:50.825532Z',
            updated_at: '2022-03-22T15:39:50.825532Z',
            title: 'task 1',
            attachments: [],
            body: 'some descripiton',
          },
          task_status: 'OPEN',
          detail_status: [
            {
              status: 'OPEN',
              count: 1,
            },
            {
              status: 'FINISHED',
              count: 0,
            },
          ],
        },
      ],
    };
  }
}
