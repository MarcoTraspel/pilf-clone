import { Controller, Get } from '@nestjs/common';

import { Message, TasksDTOCoreApiModel } from '@pilf-onboarding/api-interfaces';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('hello')
  getData(): Message {
    return this.appService.getData();
  }

  @Get('tasks')
  getTasks(): TasksDTOCoreApiModel {
    return this.appService.getTasks();
  }
}
