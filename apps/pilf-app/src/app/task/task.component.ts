import { Component, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'pilf-task',
  templateUrl: './task.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./task.component.scss']
})
export class TaskComponent {

  @Input() author = '';
  @Input() title = '';
  @Input() status = '';
  @Input() updatedAt = '';
  @Input() body = '';

}
