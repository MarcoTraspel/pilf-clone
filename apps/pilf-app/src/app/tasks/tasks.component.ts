import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TasksDTOCoreApiModel } from '@pilf-onboarding/api-interfaces';
import { Observable } from 'rxjs';

@Component({
  selector: 'pilf-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  tasks$!: Observable<TasksDTOCoreApiModel>;

  constructor(private http: HttpClient) { 
    
  }

  ngOnInit(): void {
    this.tasks$ = this.http.get<TasksDTOCoreApiModel>('/api/tasks');
    this.tasks$.subscribe(res => {
      console.log(res);
    })
  }

}
