export interface Message {
  message: string;
}

export * from './attachmentDTO';
export * from './detailStatusDTO';
export * from './distributedTaskDTO';
export * from './receivedTaskDTO';
export * from './recipientUserDTO';
export * from './taskInformationDTO';
export * from './tasksDTO';
export * from './taskStatusDTO';
