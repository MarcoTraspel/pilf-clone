/**
 * Core
 * Core-API for content, groups and users
 *
 * The version of the OpenAPI document: 0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


/**
 * Current Status of Task from authors perspective.
 */
export type TaskStatusDTOCoreApiModel = 'UPCOMING' | 'OPEN' | 'IN_PROGRESS' | 'FINISHED' | 'OVERDUE';

export const TaskStatusDTOCoreApiModel = {
    UPCOMING: 'UPCOMING' as TaskStatusDTOCoreApiModel,
    OPEN: 'OPEN' as TaskStatusDTOCoreApiModel,
    IN_PROGRESS: 'IN_PROGRESS' as TaskStatusDTOCoreApiModel,
    FINISHED: 'FINISHED' as TaskStatusDTOCoreApiModel,
    OVERDUE: 'OVERDUE' as TaskStatusDTOCoreApiModel
};

